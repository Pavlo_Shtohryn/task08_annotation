package com.shtohryn.implementation;

import com.shtohryn.annotation.ControlledObject;
import com.shtohryn.annotation.StartObject;
import com.shtohryn.annotation.StopObject;

@ControlledObject(name="biscuits")
public class Cookies {

    @StartObject
    public void createCookie(){
        System.out.println("Create Cookie");
    }
    @StopObject
    public void stopCookie(){
        System.out.println("Stop Cookie");
    }
}
